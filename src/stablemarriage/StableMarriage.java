/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package stablemarriage;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

/**
 *
 * @author eziama
 */
public class StableMarriage {

    List<String> groupA;
    List<String> groupB;
    Map<String, LinkedHashMap<String, Boolean>> preferences;

    public StableMarriage() {
        groupA = new ArrayList<>();
        groupA.add("Oge");
        groupA.add("Jerry");
        groupA.add("Fortune");
        groupA.add("Simon");
        groupA.add("Winifred");
        groupA.add("Gozie");
        groupB = new ArrayList<>();
        groupB.add("Victor");
        groupB.add("Favour");
        groupB.add("Collins");
        groupB.add("Ogbonna");
        groupB.add("McSam");
        groupB.add("Eziama");
        preferences = new HashMap<>();
        // for holding each person's preferences
        LinkedHashMap<String, Boolean> pref = new LinkedHashMap<>();
        // Eziama's preference
        pref.put(groupA.get(5), false);
        pref.put(groupA.get(0), false);
        pref.put(groupA.get(3), false);
        pref.put(groupA.get(1), false);
        pref.put(groupA.get(4), false);
        pref.put(groupA.get(2), false);
        // now store the preference with the person's name
        preferences.put(groupB.get(5), pref);
        // McSam's preference
        // we can now reuse the pref object, since we've saved out the last one we used
        pref = new LinkedHashMap<>();
        pref.put(groupA.get(3), false);
        pref.put(groupA.get(5), false);
        pref.put(groupA.get(1), false);
        pref.put(groupA.get(2), false);
        pref.put(groupA.get(0), false);
        pref.put(groupA.get(4), false);
        preferences.put(groupB.get(4), pref);
        // Ogbonna's preference
        pref = new LinkedHashMap<>();
        pref.put(groupA.get(2), false);
        pref.put(groupA.get(4), false);
        pref.put(groupA.get(0), false);
        pref.put(groupA.get(1), false);
        pref.put(groupA.get(5), false);
        pref.put(groupA.get(3), false);
        preferences.put(groupB.get(3), pref);
        // Collin's preference
        pref = new LinkedHashMap<>();
        pref.put(groupA.get(2), false);
        pref.put(groupA.get(4), false);
        pref.put(groupA.get(0), false);
        pref.put(groupA.get(1), false);
        pref.put(groupA.get(3), false);
        pref.put(groupA.get(5), false);
        preferences.put(groupB.get(2), pref);
        // Favour's preference
        pref = new LinkedHashMap<>();
        pref.put(groupA.get(0), false);
        pref.put(groupA.get(2), false);
        pref.put(groupA.get(3), false);
        pref.put(groupA.get(4), false);
        pref.put(groupA.get(1), false);
        pref.put(groupA.get(5), false);
        preferences.put(groupB.get(1), pref);
        // Victor's preference
        pref = new LinkedHashMap<>();
        pref.put(groupA.get(0), false);
        pref.put(groupA.get(4), false);
        pref.put(groupA.get(3), false);
        pref.put(groupA.get(1), false);
        pref.put(groupA.get(2), false);
        pref.put(groupA.get(5), false);
        preferences.put(groupB.get(0), pref);
        // Group A's preferencesz
        // Gozie's preference
        pref = new LinkedHashMap<>();
        pref.put(groupB.get(2), false);
        pref.put(groupB.get(5), false);
        pref.put(groupB.get(0), false);
        pref.put(groupB.get(3), false);
        pref.put(groupB.get(1), false);
        pref.put(groupB.get(4), false);
        preferences.put(groupA.get(5), pref);
        // Winifred's preference
        pref = new LinkedHashMap<>();
        pref.put(groupB.get(5), false);
        pref.put(groupB.get(0), false);
        pref.put(groupB.get(3), false);
        pref.put(groupB.get(2), false);
        pref.put(groupB.get(4), false);
        pref.put(groupB.get(1), false);
        preferences.put(groupA.get(4), pref);
        // Simon's preference
        pref = new LinkedHashMap<>();
        pref.put(groupB.get(5), false);
        pref.put(groupB.get(0), false);
        pref.put(groupB.get(4), false);
        pref.put(groupB.get(1), false);
        pref.put(groupB.get(2), false);
        pref.put(groupB.get(3), false);
        preferences.put(groupA.get(3), pref);
        // Fortune's preference
        pref = new LinkedHashMap<>();
        pref.put(groupB.get(5), false);
        pref.put(groupB.get(0), false);
        pref.put(groupB.get(2), false);
        pref.put(groupB.get(3), false);
        pref.put(groupB.get(4), false);
        pref.put(groupB.get(1), false);
        preferences.put(groupA.get(2), pref);
        // Jerry's preference
        pref = new LinkedHashMap<>();
        pref.put(groupB.get(0), false);
        pref.put(groupB.get(3), false);
        pref.put(groupB.get(2), false);
        pref.put(groupB.get(1), false);
        pref.put(groupB.get(4), false);
        pref.put(groupB.get(5), false);
        preferences.put(groupA.get(1), pref);
        // Oge's preference
        pref = new LinkedHashMap<>();
        pref.put(groupB.get(0), false);
        pref.put(groupB.get(5), false);
        pref.put(groupB.get(1), false);
        pref.put(groupB.get(4), false);
        pref.put(groupB.get(2), false);
        pref.put(groupB.get(3), false);
        preferences.put(groupA.get(0), pref);
    }

    public void printStatus() {
        System.out.println("Group A\t\tGroup B");
        for (int i = 0; i < groupA.size(); i++) {
            System.out.println(groupA.get(i) + "\t\t" + groupB.get(i));
        }
        System.out.println("======== PREFERENCES ============== ");
        for (String name : preferences.keySet()) {
            Map<String, Boolean> value = preferences.get(name);
            System.out.println("\n========" + name + "'s Preferences =========");
            for (String n : value.keySet()) {
                System.out.print(n + ",  ");
            }
            System.out.println();
        }
    }

    public void printEngagement(Map<String, String> engagementList) {
        System.out.println("=========== THE ENGAGEMENT LIST =============");
        for (String m : engagementList.keySet()) {
            System.out.println(m + " => " + engagementList.get(m));
        }
    }

    public void stableMatching() {
        // a map that holds the free status of each person. True = free
        Map<String, Boolean> status = new HashMap<>();
        Map<String, String> engagementList = new HashMap<>();
        // initialize everyone to single, i.e. free
        for (int i = 0; i < groupA.size(); i++) {
            status.put(groupA.get(i), true);
            status.put(groupB.get(i), true);
        }
        System.out.println("");
        while (engagementList.size() < groupA.size()) {
            for (String m : groupA) {
                if (status.get(m)) {// this means a is free, because status = true means free
                    // so we should go through the preference list for a, and find
                    // the first woman who has not been proposed to
                    for (Map.Entry<String, Boolean> b : preferences.get(m).entrySet()) {
                        if (b.getValue() == false) { // same as if(!b.getValue())
                            // this means this woman has not been proposed to
                            // now do the rest of the algorithm!!!!! Yaaaay. Udo nu!!
                            String w = b.getKey(); // this is a woman that hasn't been proposed to
                            // mark this woman as having been proposed to now
                            preferences.get(m).replace(w, true);
                            if (status.get(w)) { // i.e. if(status.get(w)==true)
                                // it means this woman is still free
                                engagementList.put(m, w);
                                // m and w are no more single and free, so update the status
                                status.put(m, false);
                                status.put(w, false);
                                break;
                            }
                            else {
                                // this is the other guy who caught w first
                                String m2 = null;
                                for (String mm : engagementList.keySet()) {
                                    String v = engagementList.get(mm);
                                    if (v.equals(w)) {
                                        m2 = mm;
                                        break;
                                    }
                                }
                                // prepare for the position of the men in w's life
                                int mPosition = -1;
                                int m2Position = -1;
                                int counter = 0;
                                // now check if the woman w prefers m to m2
                                for (Map.Entry<String, Boolean> a : preferences.get(w).entrySet()) {
                                    if (a.getKey().equals(m)) {
                                        mPosition = counter;
                                    }
                                    else if (a.getKey().equals(m2)) {
                                        m2Position = counter;
                                    }
                                    if (mPosition >= 0 && m2Position >= 0) {
                                        break;
                                    }
                                    counter++;
                                }
                                // now check who is preferred
                                if (mPosition < m2Position) {
                                    // this means m is preferred to m2
                                    // so disengage m2 and w, and rather engage m and w
                                    engagementList.put(m, w);
                                    engagementList.remove(m2);
                                    // m2 should become free again
                                    status.put(m2, true);
                                    status.put(m, false);
                                    break;
                                }
                            }
                        }
                    }
                }
            }
            printEngagement(engagementList);
        }
        printEngagement(engagementList);
    }

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        StableMarriage marriage = new StableMarriage();
        marriage.printStatus();
        marriage.stableMatching();
    }

}
